import yaml
import os


class ConfigReader:
    """
    Class to parse the configuration file and build the inputs and outputs MQTT topics of each device ont the sites
    This mainly needed to provide the front
    """
    def __init__(self, config_file):
        """
        Initialize the configuration with the specified config file
        :param config_file: the configuration file of all the sites
        """
        self.config_file = config_file
        self.site = ''
        self.content = None
        self.last_mtime = 0

    def is_modified(self):
        """
        Checks if the file has been modified since the last load
        :return: True if the file has been modified since the last load
        """
        stat = os.stat(self.config_file)
        if stat.st_mtime > self.last_mtime:
            return True
        else:
            return False

    def load(self):
        """
        Load the configuration file and get its content
        :return:
        """
        with open(self.config_file, 'r') as stream:
            try:
                stat = os.stat(self.config_file)
                self.last_mtime = stat.st_mtime
                self.content = yaml.load(stream, Loader=yaml.FullLoader)
                self.site = self.content['site']
            except yaml.YAMLError as exc:
                print(exc)

    def get_customers_by_site(self, site):
        """
        Get the customers description of the site
        :param site: the name of the site
        :return: the array of the representations of the devices list in the given site
        """
        customers_ids = list()

        s = self.content['site']
        if site.lower() == s.lower():
            customers = self.content['customers']
            customers_ids = [customer['id'] for customer in customers]
        return customers_ids

    def get_devices_by_customer(self, site, customer_id):
        """
        Get the devices description of the site
        :param site: the name of the site
        :param customer_id: the id of the given customer
        :return: the array of the representations of the devices list in the given customer
        """
        devices = list()

        s = self.content['site']
        if site.lower() == s.lower():
            customers = self.content['customers']
            customer = next(filter(lambda x: x['id'] == customer_id, customers), {})
            if customer:
                devices = customer['devices']
                self._generate_topics(site, customer_id, devices)
        return devices

    def get_device_by_id(self, site, id):
        """
        Get the device information found in the site configuration file
        :param site: the name of the site
        :param id: logical id of the device
        :return: the device representation with the information contained in the site configuration file
        """
        device = None
        customer_id = None
        s = self.content['site']
        if site.lower() == s.lower():
            customers = self.content['customers']
            for c in customers:
                devices = c['devices']
                device = next(filter(lambda x: x['id'] == id, devices), None)
                if device:
                    customer_id = c['id']
                    break
        return customer_id, device

    @staticmethod
    def _generate_topics(site, customer_id, devices):
        for device in devices:

            if 'inputs' in device:
                for input in device['inputs']:
                    topic = 'tabede/'+site+'/'+str(customer_id)+'/bmse/deviceinfo/dynamic/'+str(device['id'])+'/'+input['name']
                    input['topic'] = topic

            if 'outputs' in device:
                for output in device['outputs']:
                    topic = 'tabede/'+site+'/'+str(customer_id)+'/bmse/deviceinfo/dynamic/'+str(device['id'])+'/'+output['name']
                    output['topic'] = topic



