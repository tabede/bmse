import datetime
import json
import time
from threading import current_thread
import logging


class Operation:
    def __init__(self, device_id, consumption):
        self.device_id = device_id
        self.consumption = consumption


class OperatingSchedule:
    def __init__(self, payload):
        if payload and 'time' in payload and 'data' in payload and 'period' in payload:
            # parse the payload
            period_sec = payload['period']*60
            nb_of_actions_for_one_day = 60 * 60 * 24 // period_sec
            date_time_str = payload['time']
            schedule = payload['data']
            date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%dT%H:%M:%S%z')
            start_time = date_time_obj.timestamp()

            curr_time_stamp = int(time.time())
            time_stamp = start_time
            self.operations = list()
            for i in range(nb_of_actions_for_one_day):
                for item in schedule:
                    if int(time_stamp) >= int(curr_time_stamp):  # Forget about actions scheduled in the past
                        operation = dict()
                        operation['time_stamp'] = int(time_stamp)
                        operation['device'] = item['device']
                        operation['endpoint'] = item['command']
                        if len(item['values']) > i:
                            operation['value'] = item['values'][i]
                            self.operations.append(operation)
                        else:
                            logging.warning('Missing values for device ' + item['device'])
                time_stamp += period_sec

    def _pop_next_operation(self):
        curr_time_stamp = int(time.time())
        operation = None
        if hasattr(self, 'operations'):
            logging.info("Number of operations: " + str(len(self.operations)))
            for op in reversed(self.operations):
                if op['time_stamp'] < int(curr_time_stamp):
                    if op['time_stamp'] < int(curr_time_stamp) - 60:
                        logging.warning('Scheduled action is older than 1 minute')
                    # Save the next operation to be applied (will be returned)
                    operation = op
                    # ... and remove it from the list
                    self.operations.remove(op)
                    break
        return operation

    def current_operations(self):
        """
        Generator for iterating on all operations that need to be applied currently
        """
        while True:
            operation = self._pop_next_operation()
            if not operation:
                break
            yield operation
