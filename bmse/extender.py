#!/usr/bin/env python3

from sys import stdout
import logging
import os
from threading import Event
import time
import json
from xemway import Adapter, option, Signals, signal_handler, subscribe
from bmse import ConfigReader
from bmse import OperatingSchedule

# To be started like this: python3 bmse.py --host=tabede-broker.duckdns.org --port=1884 --period=10

stop_event = Event()


@signal_handler((Signals.SIGINT, Signals.SIGTERM))
def handler_stop_signals(signum, _frame):
    logging.info('Caught signal ' + str(signum))
    stop_event.set()
    time.sleep(0.5)
    os.kill(os.getpid(), Signals.SIGKILL)


@option('-s', '--sitefile', default='site-config.yaml', show_default=True, help='Test site config file')
@option('-P', '--period', default=5, show_default=True, help='Operating scheduler period')
class Bmse(Adapter):

    def __init__(self, ):
        """
        Initialize the adapter and configuration reader
        """
        Adapter.__init__(self)
        self.operating_schedule = None
        self.site = None

    @subscribe('tabede/+/+/abo/schedule')
    def operating_schedule_handler(self, topic, payload):
        if self.site is not None and topic.split('/')[1] != self.site:
            logging.info('New schedule is for site ' + topic.split('/')[1] + ' but we manage site ' + self.site)
            return

        logging.info('topic: ' + topic + ', operating schedule: ' + str(payload))
        # refresh the new schedule as it is received every 15 min for next 24 hours
        try:
            schedule = json.loads(payload)
            self.operating_schedule = OperatingSchedule(schedule)
        except ValueError as ex:
            logging.error(str(ex))

    @staticmethod
    def _publish_devices_list(site, customer_id, devices):
        """
        Publish the complete list of the devices in the site
        :param devices: the site's devices
        :return:
        """
        logging.info('publishing to tabede/'+site+'/'+str(customer_id)+'/bmse/devicelist')
        devices_json = json.dumps(devices)
        bmse.publish('tabede/'+site+'/'+str(customer_id)+'/bmse/devicelist', devices_json, retain=True)

    @staticmethod
    def _publish_static_info(site, customer_id, devices):
        """
        Publish the static info of the devices in the site. Currently, only the flexibility is sent
        :param devices: the concerned devices of the site
        :return:
        """
        for device in devices:
            flexibility = device.get('flexibility')
            if flexibility:
                topic = 'tabede/'+site+'/'+str(customer_id)+'/bmse/deviceinfo/static/'+str(device.get('id'))
                logging.info('publishing  into ' + topic)
                flexibility_json = json.dumps(flexibility)
                bmse.publish(topic, flexibility_json, retain=True)

    def main(self):
        success = self.connect()
        if not success:
            logging.error('Failed to connect to messaging service')
            return

        # initialize the ConfigReader and make sure Bmse knows which site to manage
        conf_reader = ConfigReader(self.sitefile)
        conf_reader.load()
        self.site = conf_reader.site
        conf_reader.last_mtime = 0  # make sure device list is published at least once
        while not stop_event.wait(self.period):
            # publish only if the config file is modified
            if conf_reader.is_modified():
                conf_reader.load()
                customers_id = conf_reader.get_customers_by_site(conf_reader.site)
                for customer_id in customers_id:
                    devices = conf_reader.get_devices_by_customer(conf_reader.site, customer_id)
                    self._publish_devices_list(conf_reader.site.lower(), customer_id, devices)
                    self._publish_static_info(conf_reader.site.lower(), customer_id, devices)

            # Apply the following scheduled operations using the local broker
            if self.operating_schedule:
                for operation in self.operating_schedule.current_operations():
                    # apply the current operation
                    if operation:
                        # get the device from the config file
                        customer_id, device = conf_reader.get_device_by_id(conf_reader.site, operation['device'])
                        if device:
                            # fire the command to the concerned set point
                            topic = 'tabede/' + conf_reader.site.lower() + '/' + str(customer_id) +\
                                    '/bmse/command/' + device['id'] + '/' + operation['endpoint']
                            set_query = dict()
                            set_query['command'] = 'SET'
                            set_query['body'] = operation['value']
                            response = self.request(topic, set_query, timeout=5)
                            logging.info('Sent query ' + str(set_query) + ' to topic: ' + topic +
                                         '. Got response: ' + str(response))
        self.disconnect()
        logging.info('exiting...')


bmse = Bmse()


def main():
    # Set the logging level and format
    FORMAT = '%(asctime)-15s %(levelname)-8s %(filename)-15s %(funcName)-20s %(message)s'
    logging.basicConfig(stream=stdout, level=logging.INFO, format=FORMAT)
    logging.info('starting...')

    bmse()


if __name__ == '__main__':
    main()
