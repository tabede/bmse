# BMSE
The BMSE is the central process that will provide the basic information about the devices in the site and the static information used for different algorithms.

# Prequisites
The host where the information shall be published should run `mosquitto`

# Installing
```commandline
pip3 install git+https://github.csem.local/tabede/bmse.git
```
# Running
To run the process linked with public broker running on `tabede-broker.duckdns.org`

```commandline
cd path/to/bmse
python3 bmse.py -h tabede-broker.duckdns.org -p 1884 -c credentials.yaml -s site-config.yaml
```

The process will publish the device information in the topic `tabede/site/customer_id/bmse/devicelist`

The process will also publish the static information, if present, of each device in the site in `tabede/site/customer_id/bmse/deviceinfo/device_id/staticinfo`

These information will published with `retain=True` policy in order to be consumed any time.

These information will be re-published once a change is detected in the .yaml configuration file

# Config File

The configuration file, e.g. `site-config.yaml` contains the topology of the BMS devices in the current site of implementation.

An example of the configuration file:

```yaml

site: bergamo
name: Bergamo
customers:
  - id: demo
    name: Bergamo Demo
    devices:
      - id: '8449'
        address: 4/1/1
        description: Front Brightness
        user_id: 1
        flexibility:
          enabled: false
        technology: spacelynk
        adapter: adapter-spacelynk
        location: room1
        inputs: []
        outputs:
          - name: illuminance
            type: illuminance
            unit: lx
            period: 60
      - id: '8450'
        address: 4/1/2
        description: Left Brightness
        user_id: 1
        flexibility:
          enabled: false
        technology: spacelynk
        adapter: adapter-spacelynk
        location: room1
        inputs: []
        outputs:
          - name: irradiance
            type: irradiance
            unit: lx
            period: 60
```

**Note**: the yaml filename and the `site` field in the yaml must correspond. E.g. for the above example, the filename must be `bergamo.yaml`.

# Schedule
The operating schedule contains the commands to be fired to each device using the appropriate topic.

The schedule uses the start time and the period to send a new command. 

An example of the schedule received on the topic `tabede/+/+/abo/schedule`

```json
{
    "siteId": "bergamo_testbench",
    "customerId": "sei",
    "period": 60,
    "data": [
        {
            "device": "1000001",
            "command": "temperature",
            "values": [20, 20, 20, 20, 20, 20,
                       22, 23, 24, 24, 24, 24,
                       24, 24, 24, 24, 24, 24,
                       20, 20, 20, 20, 20, 20]
        },
        {
            "device": "1000004",
            "command": "sw1",
            "values": [0, 0, 0, 0, 0, 0,
                       1, 1, 1, 0, 0, 0,
                       0, 0, 0, 1, 1, 0,
                       0, 0, 0, 0, 0, 0]
        }
    ],
    "time": "2019-06-26T14:18:00+02:00"
}

```
# SystemD service

The process can be run in the SystemD by generation the file `tabede-bmse.service` as in the example.

The link of the different files and executables should be absolute.

````ini
[Unit]
Description=TABEDE bmse
After=multi-user.target
Requires=mosquitto.service

[Service]
Type=simple
ExecStart=/usr/bin/python3 /absolute/path/to/bmse/bmse.py -h host -p port_number  -c cred_file -s site_file
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target

````
