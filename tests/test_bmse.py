#!/usr/bin/env python3

from sys import stdout
import logging
from threading import Event
import time
import json
from xemway import Adapter, Cli, Messaging, option, query, Signals, signal_handler
import subprocess
import threading


# To be started like this: python3 bmse.py --host=tabede-broker.duckdns.org --port=1884 --period=10


@signal_handler((Signals.SIGINT, Signals.SIGTERM))
def handler_stop_signals(signum, _frame):
    logging.info('Caught signal ' + str(signum))
    stop_event.set()
    time.sleep(0.5)


# The following option names were chosen so that it matches the Messaging property names
@option('-h', '--host', default='127.0.0.1', show_default=True, help='Messaging host broker name')
@option('-p', '--port', default=1883, show_default=True, help='Messaging port broker number')
@option('-c', '--cred', help='Messaging broker credentials YAML file')
class LocalAdapter(Cli, Messaging):
    def __init__(self):
        Cli.__init__(self)
        # the default address of the internal broker is local host
        Messaging.__init__(self)


class TestAdapter(LocalAdapter):

    def __init__(self, ):
        LocalAdapter.__init__(self)
        self.started = False

    #@query('tabede/+/+/bmse/command/+/+')
    def commands_handler(self, topic, payload):
        response = dict()
        logging.info('topic: ' + topic + ', got message: ' + str(payload))

        response['endpoint'] = payload['endpoint']
        response['response'] = 'ok'

        return str(response)

    def stop(self):
        self.started = False

    def _run(self):
        success = self.connect()
        if not success:
            logging.error('Failed to connect to messaging service')
            return
        self.started = True

        while self.started:
            time.sleep(1)

        self.disconnect()
        logging.info('exiting...')

    def start(self):
        thread = threading.Thread(target=self._run)
        thread.start()


@option('-h', '--host', default='tabede-broker.duckdns.org', show_default=True, help='Messaging host broker name')
@option('-p', '--port', default=1884, show_default=True, help='Messaging port broker number')
@option('-c', '--cred', default='credentials.yaml', help='Messaging broker credentials YAML file')
@option('-P', '--period', default=5, show_default=True, help='Period [s] between two publish')
class TestAbo(Adapter):
    agenda = {
        "buildingID": "1000004",
        "period": 60,
        "schedule": [
            {
                "device": "1000001",
                "command": "temperature",
                "values": [20, 20, 20, 20, 20, 20,
                           22, 23, 24, 24, 24, 24,
                           24, 24, 24, 24, 24, 24,
                           20, 20, 20, 20, 20, 20]
            },
            {
                "device": "1000004",
                "command": "sw1",
                "values": [0, 0, 0, 0, 0, 0,
                           1, 1, 1, 0, 0, 0,
                           0, 0, 0, 1, 1, 0,
                           0, 0, 0, 0, 0, 0]
            }
        ],
        "time": "2019-06-26T14:18:00"
    }

    def __init__(self, ):
        Adapter.__init__(self)
        self.agent = TestAdapter()
        self.agent.start()

    @staticmethod
    def _publish_operating_schedule(operatingschedule):
        logging.info('publishing to tabede/cardiff/abo/operatingschedule')
        devices_json = json.dumps(operatingschedule)
        adapter.publish('tabede/cardiff/abo/operatingschedule', devices_json, retain=True)

    def main(self):
        success = self.connect()
        if not success:
            logging.error('Failed to connect to messaging service')
            return

        self._publish_operating_schedule(operatingschedule=self.agenda)

        while not stop_event.wait(3600):
            logging.info('...')
            self._publish_operationg_schedule(operatingschedule=self.agenda)

        self.disconnect()
        logging.info('exiting...')


if __name__ == '__main__':
    # Set the logging level and format
    FORMAT = '%(asctime)-15s %(levelname)-8s %(filename)-15s %(funcName)-20s %(message)s'
    logging.basicConfig(stream=stdout, level=logging.INFO, format=FORMAT)
    logging.info('starting...')

    # start the bmse process
    # subprocess.Popen(['/usr/local/bin/python', '../bmse.py', '-h', 'raspberryzero.csem.local'])
    subprocess.Popen(['/usr/local/bin/python', '../bmse/bmse.py', '-h', 'localhost'])

    # To test: mosquitto_sub -h tabede-broker.duckdns.org -p 1884 -u dev -P devdev -v  -t tabede/csem/#
    #subprocess.Popen(['/usr/local/bin/python', '/Users/afa/Projects/Tabede/sw/bmse/bmse/bmse.py', '-h',
    #                  'tabede-broker.duckdns.org', '-p', '1884', '-c', 'credentials.yaml'])

    # start the test program
    stop_event = Event()
    adapter = TestAbo()
    adapter()
