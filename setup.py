from setuptools import find_packages
from setuptools import setup


setup(
    name='bmse',
    version='1.1',
    author="FALHI Abdessamad",
    author_email="abdessamad.falhi@csem.ch",
    packages=['bmse'],
    include_package_data=True,
    install_requires=[
        'pyyaml',
        'xemway@git+https://github.csem.local/xemway/xemway-python.git',
    ],
    entry_points={
        'console_scripts': [
            'bmse = bmse.extender:main'
        ],
    },
)
